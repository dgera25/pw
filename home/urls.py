from django.urls import path

from home import views

urlpatterns = [
    path('1', views.index, name = "index_view"),
    path('2', views.index2, name = "index2_view"),
    path('3', views.index3, name = "index3_view"),
    path('4', views.index4, name = "index4_view"),
    path('5', views.index5, name = "index5_view"),
    path('6', views.index6, name = "index6_view"),
    path('7', views.index7, name = "index7_view"),
    path('8', views.index8, name = "index8_view"),
    path('9', views.index9, name = "index9_view"),
    path('10', views.index10, name = "index10_view"),
]