from django.db import models

# Create your models here.
class Jugador(models.Model):
	nombre_de_jugador = models.CharField(max_length=150)
	posicion = models.CharField(max_length=36)
	no_de_jugador = models.IntegerField()
	equipo = models.CharField(max_length=55)
	estatus = models.CharField(max_length=36)

	def __str__(self):
		return self.nombre_de_jugador

class Equipo(models.Model):
	nombre_del_equipo = models.CharField(max_length=32)
	no_de_jugadores = models.IntegerField()
	ciudad_de_sede = models.CharField(max_length=32)

	def __str__(self):
		return self.nombre_del_equipo

class Estadio(models.Model):
	nombre = models.CharField(max_length=36)
	direccion = models.CharField(max_length=60)
	equipo_local = models.CharField(max_length=60)

	def __str__(self):
		return self.nombre
